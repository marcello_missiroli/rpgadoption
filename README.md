README
======

A short study exploring how adoption of a new product occur in a very spercific market - tabletop roleplaying game systems. Its peculiar social structure makes it difficult to predict whether a new product will effectively become a success. Real data is gathered by accessing data on Google trends, rpg stack exchange, reddit and rpggeek.com. A simple model representing adoption evolution is proposed.

You can have a look [at the final worke here](https://bitbucket.org/marcello_missiroli/rpgadoption/src/d1fa4624c7124cfac69e5d9b05c1fdeb5afeba48/paper/rpgadoption.pdf?at=master&fileviewer=file-view-default)