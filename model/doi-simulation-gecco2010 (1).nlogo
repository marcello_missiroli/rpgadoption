breed [nodes node]

nodes-own [node-id rho adopted? appeal?
    my-apl     ; average-path-length to all other nodes in the network
    my-cc      ; clustering coefficient
    my-degree  ; what is degree (nodes reachable within 1 hop)
    my-twostep ; how many other nodes are reachable within two network hops
    my-random  ; a random number, used for choosing nodes randomly
    
    ; normalized versions of the above metrics
    norm-stat-degree
    norm-stat-inverted-apl
    norm-stat-inverted-cc
    norm-stat-twostep
    norm-stat-random
]

globals [adopted-per-tick npv discount-value seed-degree-sum done? ]

to setup
  clear-all
  reset-ticks
  set-default-shape nodes "circle"
  
  load-network network-file ;"random100_k4_test.txt"
;  ifelse import? [
;    let filename ""
;    ifelse directory = "pa/" [
;      set filename (word directory (first-half / 2) "-" second-half ".txt")
;    ]
;    [
;      set filename (word directory first-half "-" second-half ".txt")
;    ]
;    import-links filename
;  ]
 
  prepare-for-run
    
  if (use-visuals?)
  [
    with-local-randomness [
      layout-circle (sort nodes) (max-pxcor - 1)
      do-plotting
    ]
  ]
end

to prepare-for-run
    ask nodes [
    set color white
    set rho global-rho
    set adopted? false
    set appeal? false
    set size 0.5
  ]
  
  ask n-of (appeal-fraction * count nodes) nodes [
    set appeal? true
  ]

  reset-ticks
  clear-plot
  set done? false

  ;; this sets the npv to 0
  set npv 0  
  
  ;; set the discount value
  set discount-value 0.9
    
  ;; seed the network with adopters based on seeding-method
  seed-network
end

;; this seeds the network with adopters on the basis of seeding-method
to seed-network
  ask nodes [ 
    set my-random random-float 1.0
  ]
  let max-degree max [ count link-neighbors ] of nodes
  let max-apl max [ my-apl ] of nodes
  let max-cc max [my-cc ] of nodes
  let max-twostep max [my-twostep] of nodes
  let max-random max [ my-random] of nodes
  
  ask nodes [
    set norm-stat-degree (count link-neighbors) / max-degree
    set norm-stat-twostep my-twostep / max-twostep
    set norm-stat-inverted-apl 1 - (my-apl / max-apl)
    set norm-stat-inverted-cc 1 - (my-cc / max-cc)
    set norm-stat-random my-random / max-random
  ]
  
  let strategy-vectors (list
    (list strat1-weight-degree strat1-weight-twostep strat1-weight-apl strat1-weight-cc strat1-weight-random)
    (list strat2-weight-degree strat2-weight-twostep strat2-weight-apl strat2-weight-cc strat2-weight-random))
  
  let num-seeds round (seeding-fraction * count nodes)
  
  repeat num-seeds [
    let strategy-vector ifelse-value (random-float 1.0 < strat2-probability) [ item 1 strategy-vectors ] [ item 0 strategy-vectors ]
     
    ask max-one-of (nodes with [ not adopted? ])
        [sum (map [ ?1 * ?2 ] 
              strategy-vector
             (list norm-stat-degree norm-stat-twostep norm-stat-inverted-apl norm-stat-inverted-cc norm-stat-random)) ]
    [ 
      adopt
    ]
  ]
     
end

;; set the color red and set the adopted? flag to true
to adopt
  set color red
  set adopted? true
end

;; this updates the net present value
;;   basically this is the net present value at time 0 for the network
to update-npv
  set npv npv + adopted-per-tick * (discount-value ^ ticks)
end

to go
  let susceptible turtles with [ not adopted? and appeal? ]
  if not any? susceptible [
    set done? true
    stop
  ]
  set adopted-per-tick 0
  ask susceptible [
    if random-float 1.0 < ( rho + v * get-exposure ) [
      adopt
      set adopted-per-tick adopted-per-tick + 1
    ]
  ]
  
  ;; update the net present value before the tick
  update-npv
  
  tick
  if (use-visuals?)
  [
    with-local-randomness [
      do-plotting
    ]
  ]
end

to-report get-exposure
  let infectible-neighbors count link-neighbors with [appeal?]
  let adopted-neighbors count link-neighbors with [ adopted? ]
  ifelse infectible-neighbors = 0 [
    report 0
  ][
    report adopted-neighbors / infectible-neighbors
  ]
end

to do-plotting
  plotxy ticks count nodes with [adopted?]
end

to layout
  let factor sqrt count nodes
  ;; numbers here are arbitrarily chosen for pleasing appearance
  repeat 10 [
    layout-spring nodes links 0.35 (7 / factor) 0.05
  ]
end


to-report file-get-next-noncomment-line 
  let line file-read-line
  while [first line = "#" and not file-at-end?]
  [
    set line file-read-line
  ]
  if file-at-end?
  [
    report ""
  ]
  report line
end
  
to load-network [ fname ]
  file-open fname
  let num-nodes read-from-string file-get-next-noncomment-line
  let num-links read-from-string file-get-next-noncomment-line
  let node-list []

  create-nodes num-nodes [
    set node-list fput self node-list    
    set size 0.5
  ]
  let ignore file-read-line ; #links
  repeat num-links
  [
    let id1 file-read
    let id2 file-read
    ask (item id1 node-list)
    [
      create-link-with (item id2 node-list)
    ]
  ]
  set ignore file-read-line ; #node stats
  repeat num-nodes [
    let nodeid file-read
    ask item nodeid node-list [
      set node-id nodeid
      set my-apl file-read
      set my-cc file-read
      set my-degree file-read
      set my-twostep file-read
    ]
  ]
  file-close
end

to-report npv-from-one-run
  prepare-for-run
  while [ not done? ]  [ go ]
  report npv 
end

to-report average-of-runs [ num-runs ]
  setup
  
  report mean n-values num-runs [  npv-from-one-run ]
end
@#$#@#$#@
GRAPHICS-WINDOW
184
10
614
461
17
17
12.0
1
12
1
1
1
0
0
0
1
-17
17
-17
17
1
1
1
ticks

BUTTON
15
10
175
43
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL

BUTTON
15
45
78
78
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL

PLOT
865
35
1065
185
Adoptions over Time
Time
Adoptions
0.0
10.0
0.0
10.0
true
false
PENS
"default" 1.0 0 -16777216 true

MONITOR
865
190
922
235
Nodes
count turtles
17
1
11

MONITOR
925
190
1012
235
# Adopters
count turtles with [adopted?]
17
1
11

SLIDER
0
265
172
298
v
v
0
1.0
0.455
0.01
1
NIL
HORIZONTAL

SLIDER
1
302
173
335
global-rho
global-rho
0
1.0
0.01535
0.015
1
NIL
HORIZONTAL

CHOOSER
10
115
175
160
network-file
network-file
"erdos_expected13343.nlgraph" "lattice_k26.nlgraph" "powerBA_k14.nlgraph" "smallworld_rewire0.01_k26.nlgraph" "twitter_bfs.nlgraph"
3

SLIDER
1
226
173
259
appeal-fraction
appeal-fraction
0
1.00
1
0.05
1
NIL
HORIZONTAL

MONITOR
1015
190
1072
235
NIL
npv
2
1
11

BUTTON
80
45
175
78
do layout
layout\ndisplay
T
1
T
OBSERVER
NIL
NIL
NIL
NIL

SLIDER
640
10
812
43
seeding-fraction
seeding-fraction
0
1.0
0.015
.001
1
NIL
HORIZONTAL

SLIDER
630
60
830
93
strat2-probability
strat2-probability
0
1
0
0.01
1
NIL
HORIZONTAL

SLIDER
630
250
830
283
strat1-weight-random
strat1-weight-random
0
1
0
0.01
1
NIL
HORIZONTAL

SLIDER
630
180
830
213
strat1-weight-apl
strat1-weight-apl
0
1
0
0.01
1
NIL
HORIZONTAL

SLIDER
630
215
830
248
strat1-weight-cc
strat1-weight-cc
0
1
0
0.01
1
NIL
HORIZONTAL

SLIDER
630
145
832
178
strat1-weight-twostep
strat1-weight-twostep
0
1
0
0.01
1
NIL
HORIZONTAL

SLIDER
630
110
830
143
strat1-weight-degree
strat1-weight-degree
0
1
1
0.01
1
NIL
HORIZONTAL

SLIDER
630
300
830
333
strat2-weight-degree
strat2-weight-degree
0
1
1
0.01
1
NIL
HORIZONTAL

SLIDER
630
370
830
403
strat2-weight-apl
strat2-weight-apl
0
1
0
0.01
1
NIL
HORIZONTAL

SLIDER
630
405
830
438
strat2-weight-cc
strat2-weight-cc
0
1
0
.01
1
NIL
HORIZONTAL

SLIDER
630
335
830
368
strat2-weight-twostep
strat2-weight-twostep
0
1
0
.01
1
NIL
HORIZONTAL

SLIDER
630
440
830
473
strat2-weight-random
strat2-weight-random
0
1
0
0.01
1
NIL
HORIZONTAL

SWITCH
17
405
154
438
use-visuals?
use-visuals?
0
1
-1000

@#$#@#$#@
WHAT IS IT?
-----------
The code example provides an illustration of how to import network data from external files.  This is useful when you have a specific network, perhaps created in another program or taken from real world data, that you would like to recreate in NetLogo.

It imports data from two different files.  The first is the "attributes.txt" file, which contains information about the nodes -- in this case, the node-id, size, and color of each node. The second is the "links.txt" file, which contains information on how the nodes are connected and the strength of their connection.


NETLOGO FEATURES
----------------
The link primitives are used to represent and process connections between nodes.

The file primitives are used to read data from external files.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line-half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 4.1RC6
@#$#@#$#@
import-network
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="100"/>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="filename">
      <value value="&quot;lattice/3.txt&quot;"/>
      <value value="&quot;lattice/2.txt&quot;"/>
      <value value="&quot;lattice/1.txt&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rho">
      <value value="0.88"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="v">
      <value value="0.87"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="import?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="seeding-exp1" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>npv</metric>
    <metric>seed-degree-sum</metric>
    <enumeratedValueSet variable="appeal-fraction">
      <value value="1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="second-half" first="0" step="1" last="9"/>
    <enumeratedValueSet variable="global-rho">
      <value value="0.01535"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="v">
      <value value="0.455"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seeding-method">
      <value value="&quot;none&quot;"/>
      <value value="&quot;random&quot;"/>
      <value value="&quot;pa&quot;"/>
      <value value="&quot;sw&quot;"/>
      <value value="&quot;lattice&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="first-half">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="import?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="directory">
      <value value="&quot;random/&quot;"/>
      <value value="&quot;sw/&quot;"/>
      <value value="&quot;pa/&quot;"/>
      <value value="&quot;lattice/&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="seeding-exp2" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>npv</metric>
    <metric>seed-degree-sum</metric>
    <metric>avg-degree</metric>
    <enumeratedValueSet variable="appeal-fraction">
      <value value="1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="second-half" first="0" step="1" last="9"/>
    <enumeratedValueSet variable="global-rho">
      <value value="0.01535"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="v">
      <value value="0.455"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seeding-method">
      <value value="&quot;none&quot;"/>
      <value value="&quot;random&quot;"/>
      <value value="&quot;pa&quot;"/>
      <value value="&quot;sw&quot;"/>
      <value value="&quot;lattice&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="first-half">
      <value value="2"/>
      <value value="6"/>
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="import?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="directory">
      <value value="&quot;random/&quot;"/>
      <value value="&quot;sw/&quot;"/>
      <value value="&quot;pa/&quot;"/>
      <value value="&quot;lattice/&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="seeding-exp3" repetitions="10" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>npv</metric>
    <metric>seed-degree-sum</metric>
    <metric>avg-degree</metric>
    <metric>avg-path-length</metric>
    <enumeratedValueSet variable="appeal-fraction">
      <value value="1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="second-half" first="0" step="1" last="9"/>
    <enumeratedValueSet variable="global-rho">
      <value value="0.01535"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="v">
      <value value="0.455"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seeding-method">
      <value value="&quot;none&quot;"/>
      <value value="&quot;random&quot;"/>
      <value value="&quot;pa&quot;"/>
      <value value="&quot;sw&quot;"/>
      <value value="&quot;lattice&quot;"/>
      <value value="&quot;apl&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="first-half">
      <value value="2"/>
      <value value="4"/>
      <value value="6"/>
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="import?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="directory">
      <value value="&quot;random/&quot;"/>
      <value value="&quot;sw/&quot;"/>
      <value value="&quot;pa/&quot;"/>
      <value value="&quot;lattice/&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="seeding-exp4" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>npv</metric>
    <metric>seed-degree-sum</metric>
    <metric>avg-degree</metric>
    <metric>avg-path-length</metric>
    <metric>adopted-per-tick</metric>
    <enumeratedValueSet variable="appeal-fraction">
      <value value="0.1"/>
      <value value="0.5"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="second-half">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rho">
      <value value="0.008025"/>
      <value value="0.01535"/>
      <value value="0.022675"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="v">
      <value value="0.4175"/>
      <value value="0.455"/>
      <value value="0.4925"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seeding-method">
      <value value="&quot;none&quot;"/>
      <value value="&quot;random&quot;"/>
      <value value="&quot;pa&quot;"/>
      <value value="&quot;apl&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="first-half">
      <value value="2"/>
      <value value="4"/>
      <value value="6"/>
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="import?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="directory">
      <value value="&quot;random/&quot;"/>
      <value value="&quot;sw/&quot;"/>
      <value value="&quot;pa/&quot;"/>
      <value value="&quot;lattice/&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seeding-fraction">
      <value value="0.0010"/>
      <value value="0.0050"/>
      <value value="0.01"/>
      <value value="0.03"/>
      <value value="0.05"/>
      <value value="0.1"/>
      <value value="0.2"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
1
@#$#@#$#@
